
package com.example.lakshmibansal.student_system.entities;

import java.io.Serializable;

public class Student implements Serializable{

    private String name;
    private int rollno;
    public Student(String name,int rollno) {
        this.name = name;
        this.rollno = rollno;
    }

    public String getName() {

        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRollno() {
        return this.rollno;
    }

    public void setRollno(int rollno) {
        this.rollno = rollno;
    }
}
