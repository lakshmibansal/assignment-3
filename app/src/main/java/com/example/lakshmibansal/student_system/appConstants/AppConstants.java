package com.example.lakshmibansal.student_system.appConstants;

/**
 * Created by LAKSHMI BANSAL on 1/22/2015.
 */
public interface AppConstants {
    int EDIT_REQUEST = 200;
    int ADD_REQUEST = 100;
    String NULL_STRING = "";
    //colors

    String BLACK = "#000000";
    String WHITE = "#ffffff";
    String SELECTED_COLOR = "#ffff99";
    String UNSELECTED_COLOR = "#029456";

    // dropdown options
    String DEFAULT_OPTION ="Sort By";
    String SORT_BY_NAME = "Name";
    String SORT_BY_ROLLNO = "RollNo";

    //
    String VIEW_BUTTON = "View";
    String EDIT_BUTTON = "Edit";

    String UPDATE_TEXT = "Update";
    String VIEW_ACTIVITY_TITLE = "View Student";
    String EDIT_ACTIVITY_TITLE = "Edit Student";
    String CLOSE_BUTTON_TEXT = "Close";

    //messages
    String DELETE_MESSAGE = "Deleted";
    String ROLL_NO_ALREADY_EXISTS ="Roll Number Already Exists";
    String SUCCESS_MESSAGE = "Successfully Updated";
    String ENTER_CORRECT_NAME = "Enter Correct Name";
    String ENTER_CORRECT_ROLL = "Enter ROLL NO greater than zero";
    String CANCEL_MESSAGE = "Cancelled Operation!";
    String ENTER_CORRECT_NAME_SPACE = "Name cannot start with blank";

    //KEYS
    String BUTTON_KEY = "button";
    String STUDENT_KEY = "student";
    String POSITION_KEY = "position";
    String PREVIOUS_ROLL_KEY = "previous_roll";

    String DIALOG_TITLE = "Options";
}
