package com.example.lakshmibansal.student_system;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;

import android.graphics.Color;
import android.os.Bundle;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lakshmibansal.student_system.entities.Student;
import com.example.lakshmibansal.student_system.util.AddStudentActivity;
import static com.example.lakshmibansal.student_system.appConstants.AppConstants.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class MainActivity extends Activity  {
    List<Student> studentDB;
    List<Integer> rollList = new ArrayList<Integer>();
    ListAdapter adapter;
    ListView listview;
    GridView gridview;
    Button listBtn;
    Button gridBtn;
    Spinner dropdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listview = (ListView)findViewById(R.id.listView);
        gridview = (GridView) findViewById(R.id.gridView);
        listBtn = (Button)findViewById(R.id.list_option);
        gridBtn = (Button)findViewById(R.id.grid_option);
        dropdown = (Spinner)findViewById(R.id.sort_By);

        final String[] sortItems = {DEFAULT_OPTION,SORT_BY_NAME,SORT_BY_ROLLNO};
        ArrayAdapter<String> spinner_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sortItems){
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;
                if(position ==0){
                    TextView tv = new TextView(getContext());
                    tv.setHeight(0);
                    v=tv;
                }
                else{
                    v=super.getDropDownView(position,null,parent);
                }
                return v;
            }
        };
        dropdown.setAdapter(spinner_adapter);

        studentDB = new ArrayList<>();

        adapter = new ListAdapter(this,studentDB);
        listview.setAdapter(adapter);
        gridview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                click(parent,view,position,id);
            }
        });


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                click(parent,view,position,id);
            }
        });

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String choosedItem = (String)parent.getItemAtPosition(position);
                if(choosedItem.equals(SORT_BY_NAME)){
                    sortByName();
                }
                else if(choosedItem.equals(SORT_BY_ROLLNO)){
                    Collections.sort(studentDB,new Comparator<Student>() {
                        @Override
                        public int compare(Student lhs, Student rhs) {
                            return lhs.getRollno()-(rhs.getRollno());
                        }
                    });
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void onClick(View view){
        switch(view.getId()){
            case R.id.createBtn:
                openAddStudent();
                break;
            case R.id.list_option:
                listBtn.setBackgroundColor(Color.parseColor(SELECTED_COLOR));
                gridBtn.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
                listBtn.setTextColor(Color.parseColor(BLACK));
                gridBtn.setTextColor(Color.parseColor(WHITE));
                listview.setVisibility(View.VISIBLE);
                gridview.setVisibility(View.INVISIBLE);
                break;
            case R.id.grid_option:
                gridBtn.setBackgroundColor(Color.parseColor(SELECTED_COLOR));
                listBtn.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
                gridBtn.setTextColor(Color.parseColor(BLACK));
                listBtn.setTextColor(Color.parseColor(WHITE));
                gridview.setVisibility(View.VISIBLE);
                listview.setVisibility(View.INVISIBLE);
                break;
            default:
                break;
        }
    }

    public void openAddStudent(){
        Intent intent = new Intent(this,AddStudentActivity.class);
        startActivityForResult(intent, ADD_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ADD_REQUEST){
            if(resultCode == RESULT_OK){
                Student obj = (Student)data.getSerializableExtra(STUDENT_KEY);
                if(!rollList.contains(obj.getRollno())){
                    adapter.student.add(obj);
                    rollList.add(obj.getRollno());
                    sortByName();
                    Toast.makeText(this,SUCCESS_MESSAGE,Toast.LENGTH_SHORT).show();
                    adapter.notifyDataSetChanged();
                }else{
                    Toast.makeText(this,ROLL_NO_ALREADY_EXISTS,Toast.LENGTH_SHORT).show();
                }
            }
            else if(resultCode == RESULT_CANCELED){

            }

        }
        if(requestCode == EDIT_REQUEST){
            if(resultCode == RESULT_OK){
                Student obj = (Student)data.getSerializableExtra(STUDENT_KEY);
                int position = data.getIntExtra(POSITION_KEY,studentDB.size());
                int previousRoll = data.getIntExtra(PREVIOUS_ROLL_KEY,rollList.size());
                int roll = obj.getRollno();
                if(roll==previousRoll){
                    studentDB.set(position,obj);
                    sortByName();
                    Toast.makeText(this,SUCCESS_MESSAGE,Toast.LENGTH_SHORT).show();
                    adapter.notifyDataSetChanged();
                }
                else if(!rollList.contains(roll)){

                    rollList.add(roll);
                    rollList.remove(rollList.indexOf(previousRoll));
                    studentDB.set(position,obj);
                    sortByName();
                    Toast.makeText(this,SUCCESS_MESSAGE,Toast.LENGTH_SHORT).show();
                    adapter.notifyDataSetChanged();
                }else{
                    Toast.makeText(this,ROLL_NO_ALREADY_EXISTS,Toast.LENGTH_SHORT).show();
                }

            } else if(resultCode == RESULT_CANCELED){

            }
        }

        dropdown.setSelection(0);
    }

    public void click(AdapterView<?> parent, View view, final int position, long id){
        final Dialog options = new Dialog(MainActivity.this);
        options.setContentView(R.layout.item_dialog);
        options.setTitle(DIALOG_TITLE);
        Button viewBtn = (Button)options.findViewById(R.id.viewBtn);
        Button editBtn = (Button)options.findViewById(R.id.editBtn);
        Button deleteBtn = (Button)options.findViewById(R.id.deleteBtn);
        viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, AddStudentActivity.class);
                intent.putExtra(BUTTON_KEY, VIEW_BUTTON);
                intent.putExtra(STUDENT_KEY, studentDB.get(position));
                startActivity(intent);
                options.dismiss();
            }
        });
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, AddStudentActivity.class);
                intent.putExtra(BUTTON_KEY, EDIT_BUTTON);
                intent.putExtra(STUDENT_KEY, studentDB.get(position));
                intent.putExtra(POSITION_KEY, position);
                startActivityForResult(intent, EDIT_REQUEST);
                options.dismiss();
            }
        });
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rollList.remove(rollList.indexOf(studentDB.get(position).getRollno()));
                studentDB.remove(position);
                adapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this,DELETE_MESSAGE,Toast.LENGTH_SHORT).show();
                options.dismiss();
            }
        });
        options.show();
    }

    public void sortByName(){
        Collections.sort(studentDB,new Comparator<Student>() {
            @Override
            public int compare(Student lhs, Student rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
        adapter.notifyDataSetChanged();
    }
}
