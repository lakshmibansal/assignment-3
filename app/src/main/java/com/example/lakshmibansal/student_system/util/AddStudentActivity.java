package com.example.lakshmibansal.student_system.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.lakshmibansal.student_system.appConstants.AppConstants.*;
import com.example.lakshmibansal.student_system.R;
import com.example.lakshmibansal.student_system.entities.Student;

public class AddStudentActivity extends Activity {
    EditText nameText;
    EditText rollText;
    Button saveBtn,cancelBtn;
    Bundle extras;
    Student obj;
    TextView heading;
    int previousRoll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        nameText = (EditText) findViewById(R.id.name_box);
        rollText = (EditText) findViewById(R.id.roll_box);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        cancelBtn = (Button) findViewById(R.id.cancelBtn);
        heading = (TextView) findViewById(R.id.heading);
        extras = getIntent().getExtras();

        if (extras != null) {
            obj = (Student)extras.get(STUDENT_KEY);
            nameText.setText(obj.getName());
            rollText.setText(NULL_STRING+obj.getRollno());

            if (extras.getString(BUTTON_KEY).equals(VIEW_BUTTON)) {
                nameText.setEnabled(false);
                rollText.setEnabled(false);
                saveBtn.setVisibility(View.INVISIBLE);
                heading.setText(VIEW_ACTIVITY_TITLE);
                cancelBtn.setText(CLOSE_BUTTON_TEXT);
            }
            if (extras.getString(BUTTON_KEY).equals(EDIT_BUTTON)) {
                previousRoll = obj.getRollno();
                heading.setText(EDIT_ACTIVITY_TITLE);
                saveBtn.setText(UPDATE_TEXT);
            }

        }
    }
    public void onClick(View view){
        switch (view.getId()){
            case R.id.saveBtn:
                addStudent();

                break;
            case R.id.cancelBtn:
                Toast.makeText(this, CANCEL_MESSAGE, Toast.LENGTH_SHORT).show();
                setResult(RESULT_CANCELED);
                finish();
            default:
                break;
        }
    }

    private void addStudent(){
        Intent intent = new Intent();
        String name =NULL_STRING;
        String roll = NULL_STRING;
        roll = rollText.getText().toString();
        name = nameText.getText().toString();
        if(name.equals("")) {
            Toast.makeText(this, ENTER_CORRECT_NAME, Toast.LENGTH_SHORT).show();
        }else if ( roll.equals("")|| Integer.parseInt(roll)==0){
            Toast.makeText(this, ENTER_CORRECT_ROLL, Toast.LENGTH_SHORT).show();
        }else if(name.indexOf(" ")==0){
            Toast.makeText(this, ENTER_CORRECT_NAME_SPACE, Toast.LENGTH_SHORT).show();
        }
        else{
             if (extras != null && extras.getString(BUTTON_KEY).equals(EDIT_BUTTON)) {
                 obj.setName(name);
                 obj.setRollno(Integer.parseInt(roll));
                 intent.putExtra(STUDENT_KEY, obj);
                 intent.putExtra(POSITION_KEY, extras.getInt(POSITION_KEY));
                 intent.putExtra(PREVIOUS_ROLL_KEY, previousRoll);
                 setResult(RESULT_OK, intent);
                 finish();
             }else{
                    intent.putExtra(STUDENT_KEY, new Student(name, Integer.parseInt(roll)));
                    setResult(RESULT_OK, intent);
                    finish();
             }
        }
    }
}

