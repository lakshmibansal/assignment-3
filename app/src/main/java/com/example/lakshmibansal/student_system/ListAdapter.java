package com.example.lakshmibansal.student_system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lakshmibansal.student_system.entities.Student;

import java.util.List;

/**
 * Created by LAKSHMI BANSAL on 1/21/2015.
 */
public class ListAdapter extends BaseAdapter{


    List<Student> student;
    Context context;
    public ListAdapter(Context ctx,List<Student> list){
        this.student = list;
        this.context  = ctx;
    }

    @Override
    public int getCount() {
        return student.size();
    }

    @Override
    public Object getItem(int position) {
        return student.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.student_item,null);
        TextView  name_item = (TextView)view.findViewById(R.id.name_item);
        name_item.setText("Name : "+student.get(position).getName());
        TextView  roll_item = (TextView)view.findViewById(R.id.roll_item);
        roll_item.setText("Roll No : "+student.get(position).getRollno());
        return view;
    }


}
